/* Step 1: select variables using BIC criterion */

title height=7 'Rank(OUTCOME) vs. Selected Variables';

* NOTE: proc reg cannot use choose=BIC;
proc glmselect data=indata plots=all;
	model rnk_outcome = D_B1-D_B7 D_R2
		/ selection=backward(choose=BIC slstay=&alpha) 
		  stats=all details=all;
run; * eliminated: D_B1 and D_B6 (D_B5 is marginal);

/* Step 2: report significnace of coefficients */
proc reg data=indata plots=diagnostics(stats=(default bic));
	model rnk_outcome = D_B2 D_B3 D_B4 D_B5 D_B7 D_R2;
	output out=tmp_res residual=res;
quit;

/* Step 3: verify normality of residual */
proc univariate data=tmp_res normal;
	var res;
run;
