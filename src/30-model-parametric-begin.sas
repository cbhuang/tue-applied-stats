/* Beginning models - Raw */

/**
 * Non-Transformed data (Bad!)
 */

title height=7 'Parametric Model: Conc. vs. Batch';

proc reg data=indata plots=diagnostics(stats=(default bic));
	model outcome = D_B1-D_B7 D_R2;
	output out=tmp_res residual=res;
quit;  * check: the dependency of residual on OUTCOME;

* residual diagnosis;
proc univariate data=tmp_res normal;
	var res;
run;  * Shapiro-Wilk p-value < 1e-4  --> non-normal residual!;


/**
 * Transformed data (working!)
 */

title height=7 'Parametric Model: BoxCox(Conc.) vs. Batch';

proc reg data=indata plots=diagnostics(stats=(default bic));
	model bc_outcome = D_B1-D_B7 D_R2;
	output out=tmp_res residual=res;
quit;  * check: the distribution of residuals is much closer to normality!;

* residual diagnosis;
proc univariate data=tmp_res normal;
	var res;
run;  * Shapiro-Wilk p-Value = 0.7764 -> normality of residual holds!;

*--------------------- Optional Work ---------------------------
* consistency check: -0.28 is included in the 95% confidence interval of lambda estimation;
proc transreg data=indata plots=diagnostics(stats=(default bic));
	model BoxCox(outcome / lambda = -0.5 to 0 by 0.01) = identity(D_B1-D_B7 D_R2);
quit;

* the quadratic relation is not obvious, unfortunately.;
title height=7 'Parametric Model - BoxCox(Conc.) vs. Quadratic Time';
proc reg data=indata plots=diagnostics(stats=(default bic));
	model bc_outcome = time time2 D_R2;
quit;  
