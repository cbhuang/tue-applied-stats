title height=6 'Silicone Concentration by RUN';

proc sort data=raw;
	by run;
run;

proc boxplot data=raw;
	plot OUTCOME*RUN;
    insetgroup min Q1 mean Q3 max / header = 'Quantiles';
run;

