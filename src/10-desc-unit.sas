title height=6 'Silicone Concentration by UNIT';

proc sort data=raw;
	by unit;
run;

proc boxplot data=raw;
	plot OUTCOME*UNIT;
    insetgroup min Q1 mean Q3 max / header = 'Quantiles';
run;

