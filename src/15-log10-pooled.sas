/* Pooled Overview */
title height=6 'Silicone Concentration (Pooled)';

data tmp_log;
	set raw;
	log_outcome = log10(outcome);
run;

proc univariate data=tmp_log; 
	var log_outcome; 
	histogram log_outcome / endpoints = -0.4 to 1.4 by 0.1;
run;
