/** 
 * Common Names & Preprocession
 * 
 * Usage: Execute at the beginning of each SAS session
 */

/***** Libraries *****/
libname inlib "/folders/myfolders/in";
libname outlib "/folders/myfolders/out";

/***** Global Variables *****/
%let alpha=0.05;  *significance level (default=5%);
%let bc_lambda=-0.28;  * lambda for Box-Cox transformation;

/***** Preprocessed Datasets *****/

/* load raw data into RAM (reduce #I/Os) */
data raw;
	set inlib.data;
run;

/* Preprocessed variables */
* rank ordinal variables;
proc rank data=raw out=tmp_indata ties=mean;
	var outcome time;
	ranks rnk_outcome rnk_time;
run;

data indata;
	set tmp_indata;
	/* dummy for batch */
	if batch = "B1" then D_B1 = 1; else D_B1 = 0;
	if batch = "B2" then D_B2 = 1; else D_B2 = 0;
	if batch = "B3" then D_B3 = 1; else D_B3 = 0;
	if batch = "B4" then D_B4 = 1; else D_B4 = 0;
	if batch = "B5" then D_B5 = 1; else D_B5 = 0;
	if batch = "B6" then D_B6 = 1; else D_B6 = 0;
	if batch = "B7" then D_B7 = 1; else D_B7 = 0;
	/* dummy for run */
	if run = 2 then D_R2 = 1; else D_R2 = 0;
	/* quadratic time for regression */
	time2 = time**2;
	/* Box-Cox transformed OUTCOME */
	bc_outcome = (outcome**&bc_lambda - 1) / &bc_lambda;
run;

* cleanup;
proc datasets nolist;
	delete tmp_:;
quit;
