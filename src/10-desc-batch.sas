title height=6 'Silicone Concentration by BATCH';
proc boxplot data=raw;
   plot OUTCOME*BATCH;
   insetgroup min Q1 mean Q3 max /
      header = 'Quantiles';
run;

