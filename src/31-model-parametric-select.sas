/* Step 1: select variables using BIC criterion */

title height=7 'BoxCox(OUTCOME) vs. Selected Variables';

* NOTE: proc reg cannot use choose=BIC;
proc glmselect data=indata plots=all;
	model bc_outcome = D_B1-D_B7 D_R2
		/ selection=backward(choose=BIC slstay=&alpha) 
		  stats=all details=all;
run; * agrees with nonparametric model;

/* Step 2: report significnace of coefficients */
proc reg data=indata plots=diagnostics(stats=(default bic));
	model bc_outcome = D_B2 D_B3 D_B4 D_B5 D_B7 D_R2;
	output out=tmp_res residual=res;
quit;

/* Step 3: verify normality of residual */
proc univariate data=tmp_res normal;
	var res;
run;
