/* Beginning models - Ranked */

title height=7 'Rank(OUTCOME) vs. ALL';

proc reg data=indata  plots=diagnostics(stats=(default bic));
	model rnk_outcome = D_B1-D_B7 D_R2;
	output out=tmp_res residual=res;
quit;  * check: consistent and better vs. the bc-transformed parametric version!;

proc univariate data=tmp_res normal;
	var res;
run;
