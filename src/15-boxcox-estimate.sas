/* Box-Cox fit for normality */

* create a dummy variable for the right hand side of model statement
* in transreg (since this is the only easy way to fit Box-Cox in SAS); 
data tmp_fit;
	set raw(keep=outcome);
	unity = 1;
	* To remove the error of constant model, use this;
	* unity = 1 + 1e-5*rannor(1342037);
run;

* fit box-cox lambda;
proc transreg data=tmp_fit detail norestoremissing;
	model boxcox(outcome / convenient lambda = -3 to 3 by 0.01) = identity(unity);
	ods output Details=tmp_details;
run;
* store lambda value;
proc sql;
	select NumericValue into :lambda
	from tmp_details
	where Description = "Lambda Used";
quit;
%put lambda=&lambda;  *check: lambda = -0.27;

* Box-cox transform using optimized lambda; 
data tmp_bc;
	set raw;
	bc_outcome = (outcome**(&lambda) - 1) / &lambda;
run;

* test normality and visualize;
proc univariate data=tmp_bc normal;
	var bc_outcome;
	histogram bc_outcome / endpoints = -0.8 to 2.2 by 0.2;
run;

* cleanup;
proc datasets nolist;
	delete tmp_:;
quit;
