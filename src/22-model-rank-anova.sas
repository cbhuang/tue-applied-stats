title height=7 "ANOVA using proc glm";

proc glm data=indata;
    class D_B1-D_B7 D_R2;
    model rnk_outcome = D_B1-D_B7 D_R2 
        / SS2;  * partial Sum of Square for each factor;
    means D_B1-D_B7 D_R2
        / tukey cldiff;  *Tukey’s studentized range test； 95% CI;
run;


* gives similar results;
title height=7 "ANOVA using proc anova";

proc anova data=indata;
    class D_B1-D_B7 D_R2;
    model rnk_outcome = D_B1-D_B7 D_R2;
    means D_B1-D_B7 D_R2 / TUKEY CLDIFF;
run;
