/**
 * Macro - Fitting Lambda in Box-Cox transformation
 * 
 * Author: Bill Huang
 * Date: 2018-12-22
 * Ref: https://communities.sas.com/t5/SAS-Programming/How-to-stop-printing-Proc-univariate-output-NOPRINT-doesnt-work/td-p/505713
 *      (for suppressing ods output within the loop)
 */

*options nonotes nosource nosource2;  * suppress log output;

%let lambda_from=-0.6;
%let lambda_to=0;
%let lambda_step=0.01;

/* Box-cox transformation lambda fit*/
%macro fit_lambda(lambda_from, lambda_to, lambda_step);
	
	/* suppress listing to save time*/
	ods select none;
	
	/* clear previous results */
	proc datasets nolist;
		delete normstat tmp_:;
	quit;
	
	/* number of steps */
	%let N=%sysevalf((&lambda_to - &lambda_from) / &lambda_step);
	
	/* loop */
	%do i=0 %to &N;
		/* transform */
		%let lambda=%sysevalf(&lambda_from + &i * &lambda_step);
		data tmp_bc;
			SET indata;
			if &lambda = 0 then bc = log(outcome);
			else bc = (outcome**&lambda - 1) / &lambda;
		run;
		
		/* calculate P-value */
		PROC UNIVARIATE DATA=tmp_bc NORMAL;
			VAR bc;
			ODS OUTPUT TestsForNormality=tmp_bcnormality;  /* write data to work.bcnormality */
		RUN;
		
		/* filter out anderson-darling data */
		data tmp_normstat;
			set tmp_bcnormality;
			lambda = &lambda;
		run;
		/* append new data */
		proc datasets nolist;
			append base=normstat data=tmp_normstat;
		quit;
	%end;
	
	ods select all;
%mend fit_lambda;

/* execute */
%fit_lambda(&lambda_from, &lambda_to, &lambda_step);

ods graphics on;
/* check a specific statistics*/
data tmp_stat;
	set normstat(where=(test="Shapiro-Wilk"));
run;
proc print data=tmp_stat;
run;
title 'Goodness-of-fit vs. Box-Cox Lambda';
proc sgplot data=tmp_stat;
	scatter x=lambda y=stat;
run;

* cleanup;
proc datasets nolist;
	delete tmp_:;
quit;