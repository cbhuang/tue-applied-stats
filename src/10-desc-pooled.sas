/* Pooled Overview */
title height=6 'Silicone Concentration (Pooled)';

proc univariate data=raw; 
	var outcome; 
	histogram outcome / endpoints = 0 to 25 by 1;
run;
