/* Simple Levene's test */

/* Bill: may use cleaned up or transformed dataset here*/
PROC GLM DATA=inlib.data; 
CLASS batch;
MODEL outcome = batch;
MEANS batch 
	/ HOVTEST=levene;
run;
