/* Outlier detection using Grubbs */
DATA grubbs_b0;
	SET inlib.data;
	where batch = "B0";
	N = 12;
	U = (outcome - 2.653) / SQRT(9.1611);
	Grubbs = ABS(U);
	C_onesided = 2.29;
	C_twosided = 2.41;
RUN;
