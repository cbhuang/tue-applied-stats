/* Equivalence of distribution by Wilcoxon rank sum test */

/* Bill: re-written, create separate datasets */
%macro createTable(dataset, batch);
	%do i=0 %to 7;
		proc sql noprint;
			create table B&i as
			select *
			from &dataset
			where (batch="B0" or batch="B&i");
		quit;
	%end;
%mend createTable;
%createTable(inlib.data);

/* Bill: re-written, analyze */
%macro loop();
	%do i =0 %to 7;
		Title "Wilcoxon rank-sum test on batch B0 and B&i";
		PROC NPAR1WAY DATA=B&i wilcoxon correct=no;
			CLASS batch;
			VAR outcome;
			EXACT wilcoxon;
		run;
	%end;
%mend loop;
%loop;  
