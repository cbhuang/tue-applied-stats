/* Outlier detection using Tukey method */
PROC UNIVARIATE DATA=inlib.data;
	BY BATCH;
	VAR OUTCOME;
RUN;

/* BATCH0 */
DATA TUKEY_B0;
	SET inlib.data;
	WHERE BATCH="B0";  /* Bill: automatize later */
	IQR = 3.6 - 0.76;
	LOWER = 0.76 - 1.5 * IQR;
	UPPER = 3.6 + 1.5 * IQR;
	/* Bill: merged in 1 step */
	IF OUTCOME > UPPER OR OUTCOME < LOWER THEN OUTLIER =1;
	ELSE OUTLIER =0;
RUN;
