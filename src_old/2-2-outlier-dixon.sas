/* Outlier detection using DIXON */

/* Bill: Obviously incomplete! don't know her intention*/

PROC SORT DATA=inlib.data OUT=DIXON;
	BY batch outcome;
RUN;

/* Generate Dixon Bounds */
DATA dixon_bounds;  
	DIXONU_B0 =(10.16-6.64) / (10.16-0.64);
	DIXONU_B1 =(5.92-4.96) / (5.92-1.04);
	DIXONU_B2 =(24.64-18.96) / (24.64-0.72);
	DIXONU_B3 =(18.56-13.6) / (18.56-1.52);
	DIXONU_B4 =(2.24-1.52) / (2.24-0.56);
	DIXONU_B5 =(7.6-5.44) / (7.6-1.6);
	DIXONU_B6 =(8.64-4.64) / (8.64-0.96);
	DIXONU_B7 =(11.6-7.76) / (11.6-1.28);
	
	DIXONL_B0 =(0.72-0.64) / (4.8-0.64);
	DIXONL_B1 =(1.28-1.04) / (3.92-1.04);
	DIXONL_B2 =(0.88-0.72) / (15.76-0.72);
	DIXONL_B3 =(1.68-1.52) / (12.08-1.52);
	DIXONL_B4 =(0.64-0.56) / (1.28-0.56);
	DIXONL_B5 =(1.76-1.6) / (3.68-1.6);
	DIXONL_B6 =(1.12-0.96) / (2.48-0.96);
	DIXONL_B7 =(1.84-1.28) / (6.4-1.28);
RUN;
