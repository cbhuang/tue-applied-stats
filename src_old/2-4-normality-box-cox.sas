/* Normality test with transformations */

/* Bill: Box-cox transformation*/
DATA LMYDATA;
	SET inlib.data;
	l0outcome = log(outcome);  /*Bill: natural log*/
	l1outcome = (outcome**2-1) / 2;
	l2outcome = (outcome**(-1)-1) / (-1);
	l3outcome = (outcome**(-2)-1) / (-2);
	l4outcome = (outcome**(1/3)-1) / (1/3);
RUN ;
/* Bill: should explain why choose these parameters*/


/* L0 */  /*Bill: automatize later*/
title "L0";
PROC UNIVARIATE DATA=LMYDATA NORMAL;
	class BATCH;
	VAR l0outcome;
	ODS OUTPUT TestsForNormality=NORMALITYTESTL0;
RUN;
PROC PRINT DATA = NORMALITYTESTL0;
RUN ;

/* L1 */
title "L1";
PROC UNIVARIATE DATA=LMYDATA NORMAL;
	class BATCH;
	VAR l1outcome;
	ODS OUTPUT TestsForNormality=NORMALITYTESTL1;
RUN;
PROC PRINT DATA = NORMALITYTESTL1;
RUN ;

/* L2 */
title "L2";
PROC UNIVARIATE DATA=LMYDATA NORMAL;
	class BATCH;
	VAR l2outcome;
	ODS OUTPUT TestsForNormality=NORMALITYTESTL2;
RUN;
PROC PRINT DATA = NORMALITYTESTL2;
RUN ;

/* L3 */
title "L3";
PROC UNIVARIATE DATA=LMYDATA NORMAL;
	class BATCH;
	VAR l3outcome;
	ODS OUTPUT TestsForNormality=NORMALITYTESTL3;
RUN;
PROC PRINT DATA = NORMALITYTESTL3;
RUN ;

/* L4 */
title "L4";
PROC UNIVARIATE DATA=LMYDATA NORMAL;
	class BATCH;
	VAR l4outcome;
	ODS OUTPUT TestsForNormality=NORMALITYTESTL4;
RUN;
PROC PRINT DATA = NORMALITYTESTL4;
RUN ;

