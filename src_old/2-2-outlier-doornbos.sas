/* Outlier test for DOORNBOS */

/*Bill: dont know what newa2 means here*/
/*PROC UNIVARIATE DATA=newa2 NORMAL;*/

PROC UNIVARIATE DATA=inlib.data NORMAL;
	BY batch;
	VAR outcome;
run;

DATA doornbos_b0;
	SET inlib.data;
	where batch = "B0";
	N = 12;
	U = (outcome-2.653) / SQRT(9.1611);
	W = SQRT( (N * (N -2) * U**2) / ( (N-1)**2 - N * U**2) );
	doornbos_outcome = ABS(W);
	criter = QUANTILE("T", 1 - 0.05 / (2 * 12) , 10);
RUN;
