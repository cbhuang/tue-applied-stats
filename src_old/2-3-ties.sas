/* Find ties in each batch */

proc rank data=inlib.data(where=(batch="B0")) out=ranks;
	var outcome;
	ranks rnk;
run;

proc print data=ranks;
run ;
