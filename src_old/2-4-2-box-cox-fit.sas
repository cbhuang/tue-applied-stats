/**
 * Macro - Fitting Lambda in Box-Cox transformation
 * 
 * Author: Bill Huang
 * Date: 2018-12-22
 * Ref: https://communities.sas.com/t5/SAS-Programming/How-to-stop-printing-Proc-univariate-output-NOPRINT-doesnt-work/td-p/505713
 *      (for suppressing ods output within the loop)
 */
%let lambda_from=-3;
%let lambda_to=3;
%let lambda_step=0.1;

/* load raw data into RAM to reduce disk I/O */
data indata(keep=batch outcome);
	set inlib.data;
run;

/* Box-cox transformation lambda fit*/
%macro fit_lambda(lambda_from, lambda_to, lambda_step);
	
	/* suppress listing to save time*/
	ods select none;
	
	/* clear previous results */
	proc datasets nolist;
		delete bc bcnormality ad ad_new;
	quit;
	
	/* number of steps */
	%let N=%sysevalf((&lambda_to - &lambda_from) / &lambda_step);
	
	/* loop */
	%do i=0 %to &N;
		/* transform */
		%let lambda=%sysevalf(&lambda_from + &i * &lambda_step);
		data bc;
			SET indata;
			if &lambda = 0 then bc = log(outcome);
			else bc = (outcome**&lambda - 1) / &lambda;
		run;
		
		/* calculate P-value */
		PROC UNIVARIATE DATA=bc NORMAL;
			class BATCH;
			VAR bc;
			ODS OUTPUT TestsForNormality=bcnormality;  /* write data to work.bcnormality */
		RUN;
		
		/* filter out anderson-darling data */
		data ad_new;
			set bcnormality(where=(test="Anderson-Darling"));
			lambda = &lambda;
		run;
		/* append new data */
		proc datasets nolist;
			append base=ad data=ad_new;  /* base dataset need not exist initially*/
		quit;
	%end;
	
	/* show the final results only (cannot ignore this)*/
	ods select all;
%mend fit_lambda;

/* execute */
%fit_lambda(&lambda_from, &lambda_to, &lambda_step);

/* show the results on batch b4 */
proc sort data=ad(where=(batch="B4")) out=ad_b4;
	by lambda;
run;
proc print data=ad_b4;
run;
/* Check: the Stat variable reaches a bottom at a certain region */
