/* Test homogeniety of variance by Levene's and BF */

/* Bill: re-written, create table containing the 2 batches interested */
%macro createTable(dataset);
	%do i =0 %to 7;
		proc sql noprint;
			create table B&i as
			select *
			from &dataset
			where (batch="B0" or batch ="B&i") ;
		quit;
	%end;
%mend createTable;
%createTable(inlib.data);

/** Bill: test of homogeniety of variance
 *        (some errors in the code were corrected)
 */
%macro testhov(hovmethod);
	%do i =0 %to 7;
		Title "Leven - bf test to compare two groups B0 and B&i";
		PROC GLM DATA = B&i;
			CLASS batch;
			MODEL outcome = batch;
			MEANS batch 
			    / HOVTEST = &hovmethod;
			ODS OUTPUT HOVFTest = Hete_of_Var_bf_&i;
		RUN;
	%end;
%mend testhov;

/* select a method you want */
%testhov(LEVENE);  /* Bill: not "LEVENE" because SAS macro is copy-paste */
%testhov(BF);
