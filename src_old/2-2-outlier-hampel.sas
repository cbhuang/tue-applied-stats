/* Outlier detection using Hampel rule */

/** sample code for batch0 
 * (Bill: should be automatized for batch 0~7 using macro variables later)
 */

/* sort by outcome */
PROC SORT DATA=inlib.data(WHERE=(BATCH="B0")) OUT=hampel_b0;
	BY OUTCOME;
RUN;

PROC UNIVARIATE DATA=hampel_b0 NORMAL;
	VAR OUTCOME;
	where OUTCOME ^= 10.16;  /* Bill: ??*/
RUN;

/* Bill: append D value and show descript stats */
DATA Hampel_B0;
	SET Hampel_B0;
	D = ABS(OUTCOME - 1.08);
	Z = ABS(OUTCOME - 1.08) / 0.4;  /* Bill: intention? */
RUN;

PROC UNIVARIATE DATA=Hampel_B0;
	VAR D; /* Bill: may also check Z?*/
RUN ;
