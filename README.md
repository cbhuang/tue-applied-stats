# TU/e Applied Statistics Group Assignment

This is our group workspace :)


# Project Structure
* `/src`: our code is here
* `/in`: input data
* `/out`: output data and figures
* `/doc`: shared documents helpful for multiple members
* `/img`: shared images
* `/TEMP`: files not to be committed to the repository (e.g. your own TODO)

The report doc will be initialized on `overleaf` later on.

# Naming Conventions
## Libraries
* `/in`: named as `inlib`
* `/out`: names as `outlib`

## Macro Variables
* (To be added if any)

## Intermediate Results
* (To be added if any is critical)

# Utility Code for Mirroring (linux and mac only)

**WARNING: Edit the files with your own path**

**WARNING: All files in the target directory will be deleted!**

* `toSAS.sh`: mirror `src/*` from this project folder to SAS folder
* `toRepo.sh`: mirror `src/*` from SAS folder back to this project folder

# Commit Message

If you've done major revisions, you may consider writing a log file in `changelog/`
and commit by the following command:

```bash
  # commit your prepared message
  git commit -eF "changelog/vYYYY.MM.DD.HHMM.txt"
  # push to GitLab
  git push
```

Bill think this is more handy than `git commit -m` because new things always
comes out from nowhere

# Notes on Tools for Partners

This section is just for those who have no idea on how to proceed, but not to
impose any limitations to the ones who already has a comfortable way to work on.

1. For this `.md` file and `changelogs`, `Atom` text editor may be handy.
  * Preview while editing by simply pressing `Ctrl-Shift-M` with the
`Markdown Preview` package installed.
  * Integrated console window by pressing `Ctrl-``

    ![atom](img/atom.png? "Atom Interface")

2. For project structure management, I follow [this approach](https://davidwalsh.name/git-empty-directory) and [this outdated (?) approach](https://stackoverflow.com/a/20388370/3218693) and use `.gitignore` and `.gitkeep` jointly. 
